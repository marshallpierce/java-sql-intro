A demo project to show the same `WidgetDao` interface implemented a few different ways:

- `MemoryWidgetDao`: purely in memory, no database
- `JdbcWidgetDao`: plain old [JDBC](https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html)
- `JdbiWidgetDao`: [JDBI](http://jdbi.org/) with inline sql
- `JdbiSqlObjectWidgetDao`: JDBI with [Sql Object](http://jdbi.org/#_sql_objects) reflection
- `JooqWidgetDao`: [jOOQ](https://www.jooq.org/)
