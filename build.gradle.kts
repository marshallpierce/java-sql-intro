import org.flywaydb.gradle.task.FlywayCleanTask
import org.flywaydb.gradle.task.FlywayMigrateTask
import org.postgresql.Driver
import java.util.Properties

plugins {
    kotlin("jvm") version "1.3.72"
    id("org.flywaydb.flyway") version "5.2.4"
    id("nu.studer.jooq") version "3.0.3"
    id("com.github.ben-manes.versions") version "0.28.0"
    id("org.jmailen.kotlinter") version "2.4.1"
}

buildscript {
    dependencies {
        classpath("org.postgresql", "postgresql", "42.2.5")
        // used by jooq gradle plugin to write config
        classpath("com.sun.xml.bind", "jaxb-impl", "2.3.0.1")
        classpath("com.sun.xml.bind", "jaxb-core", "2.3.0.1")
        classpath("com.sun.activation", "javax.activation", "1.2.0")
    }
}

repositories {
    jcenter()
}

val deps by extra {
    mapOf(
        "slf4j" to "1.7.26",
        // also see version in buildscript
        "postgresql" to "42.2.5",
        // also see versions in buildscript
        "jaxb" to "2.3.0.1",
        "jaxbApi" to "2.3.0",
        "jdbi" to "3.14.1",
        "activation" to "1.2.0",
        "junit" to "5.4.2"
    )
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.slf4j", "slf4j-api", deps["slf4j"])
    runtimeOnly("ch.qos.logback", "logback-classic", "1.2.3")

    implementation("com.zaxxer", "HikariCP", "3.4.5")
    runtimeOnly("org.postgresql", "postgresql", deps["postgresql"])

    implementation("org.jdbi", "jdbi3-core", deps["jdbi"])
    implementation("org.jdbi", "jdbi3-kotlin-sqlobject", deps["jdbi"])

    implementation("org.jooq", "jooq")
    jooqRuntime("org.postgresql", "postgresql", deps["postgresql"])
    jooqRuntime("com.sun.xml.bind", "jaxb-impl", deps["jaxb"])
    jooqRuntime("com.sun.xml.bind", "jaxb-core", deps["jaxb"])
    jooqRuntime("javax.xml.bind", "jaxb-api", deps["jaxbApi"])
    jooqRuntime("com.sun.activation", "javax.activation", deps["activation"])

    testImplementation("org.junit.jupiter", "junit-jupiter-api", deps["junit"])
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", deps["junit"])
}

tasks.test {
    useJUnitPlatform()
}

val dbUser by extra { "sql-demo-dev" }
val dbPass by extra { "sql-demo-dev" }
val testDbUser by extra { "sql-demo-test" }
val testDbPass by extra { "sql-demo-test" }
val testDbName by extra { "sql-demo-test" }
val jdbcUrl by extra { "jdbc:postgresql://localhost:10432/sql-demo-dev" }
val testJdbcUrl by extra { "jdbc:postgresql://localhost:10432/$testDbName" }

apply(from = "jooq.gradle")

flyway {
    url = jdbcUrl
    user = dbUser
    password = dbPass
}

tasks {
    val makeTestDb by registering {
        doLast {
            val props = Properties()
            props["user"] = dbUser
            props["password"] = dbPass
            // classpath gets weird: DriverManager uses Groovy's classpath, not the gradle task classpath, so the normal way
            // won't find the driver.
            Driver().connect(jdbcUrl, props).use { conn ->
                conn.createStatement().use { stmt ->
                    val rs = stmt.executeQuery(
                            "SELECT count(*) FROM pg_catalog.pg_database WHERE datname = '$testDbName'")
                    val rowCount = if (rs.next()) {
                        rs.getInt(1)
                    } else {
                        0
                    }

                    if (rowCount == 0) {
                        // Needs to be superuser to create extensions.
                        stmt.execute("CREATE ROLE \"$testDbUser\" WITH SUPERUSER LOGIN PASSWORD '$testDbPass'")
                        stmt.execute("CREATE DATABASE \"$testDbName\" OWNER '$testDbUser'")
                    }
                }
            }
        }
    }

    val flywayCleanTest by registering(FlywayCleanTask::class) {
        url = testJdbcUrl
        user = testDbUser
        password = testDbPass

        dependsOn(makeTestDb)
    }

    val flywayMigrateTest by registering(FlywayMigrateTask::class) {
        url = testJdbcUrl
        user = testDbUser
        password = testDbPass

        dependsOn(makeTestDb)
        mustRunAfter(flywayCleanTest)
    }

    flywayMigrate {
        mustRunAfter(flywayClean)
    }

    test {
        dependsOn(flywayMigrateTest)
    }

    clean {
        dependsOn(flywayCleanTest)
    }
}
