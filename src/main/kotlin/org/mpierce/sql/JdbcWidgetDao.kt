package org.mpierce.sql

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.Statement

class JdbcWidgetDao(private val conn: Connection) : WidgetDao {
    override fun create(color: String, weight: Int): WidgetId {
        val id = conn.prepareStatement(
            """
            INSERT INTO widgets (color, weight, clicks)
            VALUES (?, ?, ?)
            RETURNING id;
            """.trimIndent(),
            Statement.RETURN_GENERATED_KEYS
        )
            .use { stmt ->
                stmt.run {
                    setString(1, color)
                    setInt(2, weight)
                    setInt(3, 0)
                    execute()
                    generatedKeys
                }.use { res ->
                    check(res.next()) { "Did not get generated key" }
                    res.getInt(1)
                }
            }

        return WidgetId(id)
    }

    override fun get(id: WidgetId): Widget? {
        return conn.prepareStatement(
            """
            SELECT * FROM widgets w WHERE w.id = ?
            """.trimIndent()
        )
            .use { stmt ->
                stmt.run {
                    setInt(1, id.id)
                    executeQuery()
                }.use { res ->
                    if (!res.next()) {
                        null
                    } else {
                        val idInt = res.getInt("id")
                        val color = res.getString("color")
                        val weight = res.getInt("weight")
                        val clicks = res.getInt("clicks")
                        val createdAt = res.getTimestamp("created_at").toInstant()

                        Widget(WidgetId(idInt), color, weight, clicks, createdAt)
                    }
                }
            }
    }

    override fun clickOn(id: WidgetId) {
        conn.prepareStatement(
            """
            UPDATE widgets w SET clicks = clicks + 1 WHERE w.id = ?
            """.trimIndent()
        )
            .use { stmt ->
                stmt.run {
                    setInt(1, id.id)
                    executeUpdate()
                }
            }
    }

    override fun totalWeightPerColor(spec: ColorSpec): Map<String, Int> {
        val (snippet, bindParameters) = when (spec) {
            AllColors -> Pair("true", { stmt -> stmt })
            is SpecificColors -> Pair(
                "w.color in (${spec.colors.joinToString(", ") { "?" }})",
                { stmt: PreparedStatement ->
                    spec.colors.forEachIndexed { index, color ->
                        stmt.setString(index + 1, color)
                    }
                    stmt
                }
            )
        }

        val sql =
            """
            SELECT w.color, sum(w.weight) as wt
            FROM widgets w
            WHERE $snippet
            GROUP BY w.color
            """.trimIndent()

        return conn
            .prepareStatement(sql)
            .use { stmt ->
                bindParameters(stmt)
                    .executeQuery()
                    .use { res ->
                        val map = mutableMapOf<String, Int>()
                        while (res.next()) {
                            map[res.getString("color")] = res.getInt("wt")
                        }
                        map.toMap()
                    }
            }
    }
}
