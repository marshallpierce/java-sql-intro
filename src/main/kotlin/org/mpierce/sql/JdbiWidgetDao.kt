package org.mpierce.sql

import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.Query
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet

class JdbiWidgetDao(private val handle: Handle) : WidgetDao {
    override fun create(color: String, weight: Int): WidgetId {
        return handle.createUpdate(
            """
            INSERT INTO widgets (color, weight, clicks)
            VALUES (?, ?, ?)
            RETURNING id
            """
        ).use { update ->
            update.bind(0, color)
                .bind(1, weight)
                .bind(2, 0)
                .executeAndReturnGeneratedKeys().let {
                    it.map { resultSet: ResultSet, _: Int, _: StatementContext ->
                        resultSet.getInt(1)
                    }
                }
                .first()
                .let { WidgetId(it) }
        }
    }

    override fun get(id: WidgetId): Widget? {
        return handle.createQuery(
            """
            SELECT * FROM widgets w WHERE w.id = ?
        """
        ).use {
            it.bind(0, id.id)
                .map(WidgetMapper())
                .findOne()
                .orElse(null)
        }
    }

    override fun clickOn(id: WidgetId) {
        handle.createUpdate(
            """
            UPDATE widgets w SET clicks = clicks + 1 WHERE w.id = ?
        """
        ).use {
            it.bind(0, id.id)
                .execute()
        }
    }

    override fun totalWeightPerColor(spec: ColorSpec): Map<String, Int> {
        val (snippet, bindParameters) = when (spec) {
            AllColors -> Pair("true", { stmt -> stmt })
            is SpecificColors -> Pair(
                "w.color in (${spec.colors.joinToString(", ") { "?" }})",
                { query: Query ->
                    spec.colors.foldIndexed(query) { index, q, color ->
                        q.bind(index, color)
                    }
                }
            )
        }

        val sql =
            """
            SELECT w.color, sum(w.weight) as wt
            FROM widgets w
            WHERE $snippet
            GROUP BY w.color
            """.trimIndent()

        return handle.createQuery(sql)
            .apply { bindParameters(this) }
            .use<Query, Map<String, Int>> { q ->
                bindParameters(q)
                    .map<Pair<String, Int>> { rs, _ ->
                        Pair(rs.getString("color")!!, rs.getInt("wt"))
                    }
                    .toMap()
            }
    }
}

class WidgetMapper : RowMapper<Widget> {
    override fun map(rs: ResultSet, ctx: StatementContext?): Widget {
        return Widget(
            WidgetId(rs.getInt("id")),
            rs.getString("color"),
            rs.getInt("weight"),
            rs.getInt("clicks"),
            rs.getTimestamp("created_at").toInstant()
        )
    }
}
