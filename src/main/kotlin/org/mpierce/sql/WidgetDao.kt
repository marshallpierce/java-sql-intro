package org.mpierce.sql

import java.time.Instant

interface WidgetDao {
    /**
     * Store a new widget
     */
    fun create(color: String, weight: Int): WidgetId

    /**
     * @return the widget with that id, or null if not found
     */
    fun get(id: WidgetId): Widget?

    /**
     * Increment the click count for the specified widget. If the id is invalid, no action is taken.
     */
    fun clickOn(id: WidgetId)

    /**
     * @return the total weight per color for the specified spec
     */
    fun totalWeightPerColor(spec: ColorSpec): Map<String, Int>
}

sealed class ColorSpec
object AllColors : ColorSpec()
data class SpecificColors(val colors: Set<String>) : ColorSpec()

data class WidgetId(val id: Int)

data class Widget(
    val id: WidgetId,
    val color: String,
    val weight: Int,
    val clicks: Int,
    val createdAt: Instant
)
