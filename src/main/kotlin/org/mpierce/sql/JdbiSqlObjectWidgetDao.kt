package org.mpierce.sql

import org.jdbi.v3.core.Handle
import org.jdbi.v3.sqlobject.config.RegisterRowMapper
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate

class JdbiSqlObjectWidgetDao(private val handle: Handle) : WidgetDao {
    override fun create(color: String, weight: Int): WidgetId {
        return WidgetId(sqlObj().create(color, weight))
    }

    override fun get(id: WidgetId): Widget? {
        return sqlObj().get(id.id)
    }

    override fun clickOn(id: WidgetId) {
        sqlObj().clickOn(id.id)
    }

    override fun totalWeightPerColor(spec: ColorSpec): Map<String, Int> {
        return JdbiWidgetDao(handle).totalWeightPerColor(spec)
    }

    private fun sqlObj() = handle.attach(SqlObjectHelper::class.java)
}

private interface SqlObjectHelper {
    @SqlUpdate(
        """INSERT INTO widgets (color, weight, clicks)
        VALUES (?, ?, 0)
        RETURNING id"""
    )
    @GetGeneratedKeys
    fun create(color: String, weight: Int): Int

    @SqlQuery("SELECT * FROM widgets w WHERE w.id = ?")
    @RegisterRowMapper(WidgetMapper::class)
    fun get(id: Int): Widget?

    @SqlUpdate("UPDATE widgets w SET clicks = clicks + 1 WHERE w.id = ?")
    fun clickOn(id: Int)
}
