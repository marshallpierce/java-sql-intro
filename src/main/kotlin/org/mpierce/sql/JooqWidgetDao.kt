package org.mpierce.sql

import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.jooq.impl.SQLDataType
import org.mpierce.sql.jooq.Tables.WIDGETS

class JooqWidgetDao(private val txnContext: DSLContext) : WidgetDao {
    override fun create(color: String, weight: Int): WidgetId {
        val record = txnContext.newRecord(WIDGETS).apply {
            this.color = color
            this.weight = weight
            clicks = 0
            store()
        }

        return WidgetId(record.id)
    }

    override fun get(id: WidgetId): Widget? {
        return txnContext.selectFrom(WIDGETS)
            .where(WIDGETS.ID.eq(id.id))
            .fetchOne()
            ?.let {
                Widget(
                    WidgetId(it.id),
                    it.color,
                    it.weight,
                    it.clicks,
                    it.createdAt.toInstant()
                )
            }
    }

    override fun clickOn(id: WidgetId) {
        txnContext.update(WIDGETS)
            .set(WIDGETS.CLICKS, WIDGETS.CLICKS.plus(1))
            .where(WIDGETS.ID.eq(id.id))
            .execute()
    }

    override fun totalWeightPerColor(spec: ColorSpec): Map<String, Int> {
        val condition = when (spec) {
            AllColors -> DSL.trueCondition()
            is SpecificColors -> WIDGETS.COLOR.`in`(spec.colors)
        }

        val sumField = DSL.sum(WIDGETS.WEIGHT)
            .coerce(SQLDataType.INTEGER)
            .`as`("wt")

        return txnContext
            .select(WIDGETS.COLOR, sumField)
            .from(WIDGETS)
            .where(condition)
            .groupBy(WIDGETS.COLOR)
            .fetchGroups(WIDGETS.COLOR)
            .mapValues { (_, record) ->
                record.first()
                    .getValue(sumField)
            }
    }
}
