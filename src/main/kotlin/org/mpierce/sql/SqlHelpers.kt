package org.mpierce.sql

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.conf.Settings
import org.jooq.impl.DSL
import javax.sql.DataSource

fun buildDataSource(
    ip: String,
    port: Int,
    dbName: String,
    user: String,
    pass: String,
    connPoolMaxSize: Int,
    connPoolMinIdle: Int
): HikariDataSource {
    val config = HikariConfig().apply {
        jdbcUrl = "jdbc:postgresql://$ip:$port/$dbName"
        username = user
        password = pass
        isAutoCommit = false
        maximumPoolSize = connPoolMaxSize
        minimumIdle = connPoolMinIdle
        // per jdbc spec, driver uses tz from the host JVM. For local dev, this is lame, so we just always set UTC.
        // This means that casting a timestamp to a date (for grouping, for instance) will use UTC.
        connectionInitSql = "SET TIME ZONE 'UTC'"
    }
    return HikariDataSource(config)
}

fun buildJooqDsl(dataSource: DataSource): DSLContext {
    return DSL.using(
        dataSource,
        SQLDialect.POSTGRES,
        Settings()
            // we're using Postgres, so we can use INSERT ... RETURNING to get db-created column values on new
            // rows without a separate query
            .withReturnAllOnUpdatableRecord(true)
    )
}
