package org.mpierce.sql

import java.time.Instant

class MemoryWidgetDao : WidgetDao {

    private val widgets: MutableMap<WidgetId, Widget> = mutableMapOf()
    private var nextId = 0

    @Synchronized
    override fun create(color: String, weight: Int): WidgetId {
        val id = WidgetId(nextId)
        nextId += 1

        widgets[id] = Widget(id, color, weight, 0, Instant.now())

        return id
    }

    @Synchronized
    override fun get(id: WidgetId): Widget? = widgets[id]

    @Synchronized
    override fun clickOn(id: WidgetId) {
        widgets[id]?.let {
            widgets[id] = it.copy(clicks = it.clicks + 1)
        }
    }

    @Synchronized
    override fun totalWeightPerColor(spec: ColorSpec): Map<String, Int> {
        val predicate: (String) -> Boolean = when (spec) {
            AllColors -> ({ true })
            is SpecificColors -> ({ c -> spec.colors.contains(c) })
        }

        return widgets
            .values
            .filter { predicate(it.color) }
            .groupBy { it.color }
            .mapValues { (_, widgets) -> widgets.map { it.weight }.sum() }
    }
}
