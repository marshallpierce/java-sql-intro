CREATE TABLE widgets
(
    id         SERIAL PRIMARY KEY,
    color      TEXT                     NOT NULL CHECK (color <> ''),
    weight     INT                      NOT NULL CHECK (weight > 0),
    clicks     INT                      NOT NULL CHECK (clicks >= 0),
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
