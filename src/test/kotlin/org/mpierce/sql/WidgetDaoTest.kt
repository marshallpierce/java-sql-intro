package org.mpierce.sql

import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.jdbi.v3.sqlobject.kotlin.KotlinSqlObjectPlugin
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

abstract class WidgetDaoTestBase {

    abstract fun runWithDao(block: (WidgetDao) -> Unit)

    @Test
    internal fun createReturnsIdThatCanBeUsedToLoadWidget() {
        runWithDao { dao ->
            val id = dao.create("blue", 123)

            val w = dao.get(id)
            assertNotNull(w)
            assertEquals(Widget(id, "blue", 123, 0, w!!.createdAt), w)
        }
    }

    @Test
    internal fun clickOnIncrementsClickCount() {
        runWithDao { dao ->
            val id = dao.create("blue", 123)

            val orig = dao.get(id)!!
            dao.clickOn(orig.id)
            val clickedOn = dao.get(id)!!

            assertEquals(orig.copy(clicks = orig.clicks + 1), clickedOn)
        }
    }

    @Test
    internal fun clickOnWithBadIdDoesNothing() {
        runWithDao { dao ->
            val id = dao.create("blue", 123)

            val orig = dao.get(id)!!
            dao.clickOn(WidgetId(-1))

            assertEquals(orig, dao.get(id))
        }
    }

    @Test
    internal fun getMissingIdReturnsNull() {
        runWithDao { dao ->
            assertNull(dao.get(WidgetId(-1)))
        }
    }

    @Test
    internal fun totalWeightPerColorAllColorsEmptyDb() {
        runWithDao { dao ->
            assertEquals(
                mapOf<String, Int>(),
                dao.totalWeightPerColor(AllColors)
            )
        }
    }

    @Test
    internal fun totalWeightPerColorAllColors() {
        runWithDao { dao ->
            dao.create("blue", 100)
            dao.create("blue", 101)
            dao.create("red", 200)
            dao.create("red", 201)

            assertEquals(
                mapOf("blue" to 201, "red" to 401),
                dao.totalWeightPerColor(AllColors)
            )
        }
    }

    @Test
    internal fun totalWeightPerColorSpecificColorsWithEveryColor() {
        runWithDao { dao ->
            dao.create("blue", 100)
            dao.create("blue", 101)
            dao.create("red", 200)
            dao.create("red", 201)

            assertEquals(
                mapOf("blue" to 201, "red" to 401),
                dao.totalWeightPerColor(SpecificColors(setOf("red", "blue")))
            )
        }
    }

    @Test
    internal fun totalWeightPerColorSpecificColorsWithSomeColor() {
        runWithDao { dao ->
            dao.create("blue", 100)
            dao.create("blue", 101)
            dao.create("red", 200)
            dao.create("red", 201)

            assertEquals(
                mapOf("red" to 401),
                dao.totalWeightPerColor(SpecificColors(setOf("red", "orange")))
            )
        }
    }

    @Test
    internal fun totalWeightPerColorSpecificColorsWithDisjointColor() {
        runWithDao { dao ->
            dao.create("blue", 100)
            dao.create("red", 200)

            assertEquals(
                mapOf<String, Int>(),
                dao.totalWeightPerColor(SpecificColors(setOf("purple", "orange")))
            )
        }
    }
}

class MemoryWidgetDaoTest : WidgetDaoTestBase() {
    override fun runWithDao(block: (WidgetDao) -> Unit) = block(MemoryWidgetDao())
}

abstract class DbTestBase : WidgetDaoTestBase() {
    val helper = DbTestHelper()

    @BeforeEach
    internal fun setUp() {
        helper.deleteAllRows()
    }

    @AfterEach
    internal fun tearDown() {
        helper.close()
    }
}

class JdbcWidgetDaoTest : DbTestBase() {
    override fun runWithDao(block: (WidgetDao) -> Unit) {
        helper.dataSource.connection.use {
            block(JdbcWidgetDao(it))
            it.commit()
        }
    }
}

class JdbiWidgetDaoTestBase : DbTestBase() {
    override fun runWithDao(block: (WidgetDao) -> Unit) {
        Jdbi.create(helper.dataSource).let { jdbi ->
            jdbi.useHandle<Exception> { handle ->
                handle.useTransaction<Exception> { txh ->
                    block(JdbiWidgetDao(txh))
                }
            }
        }
    }
}

class JdbiSqlObjectWidgetDaoTest : DbTestBase() {
    override fun runWithDao(block: (WidgetDao) -> Unit) {
        Jdbi.create(helper.dataSource).let { jdbi ->
            jdbi.installPlugin(SqlObjectPlugin())
            jdbi.installPlugin(KotlinPlugin())
            jdbi.installPlugin(KotlinSqlObjectPlugin())

            jdbi.useHandle<Exception> { handle ->
                handle.useTransaction<Exception> { txh ->
                    block(JdbiSqlObjectWidgetDao(txh))
                }
            }
        }
    }
}

class JooqWidgetDaoTest : DbTestBase() {
    override fun runWithDao(block: (WidgetDao) -> Unit) {
        helper.dslContext.transaction { c ->
            block(JooqWidgetDao(c.dsl()))
        }
    }
}
