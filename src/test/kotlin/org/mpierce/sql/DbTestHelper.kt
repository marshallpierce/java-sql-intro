package org.mpierce.sql

import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.mpierce.sql.jooq.Tables.WIDGETS
import java.io.Closeable

class DbTestHelper : Closeable {
    val dataSource: HikariDataSource = buildDataSource(
        "localhost",
        10432,
        "sql-demo-test",
        "sql-demo-test",
        "sql-demo-test",
        2,
        1
    )

    val dslContext: DSLContext

    init {
        dslContext = buildJooqDsl(dataSource)
    }

    fun deleteAllRows() {
        dslContext.transaction { c ->
            c.dsl().apply {
                batch(
                    deleteFrom(WIDGETS)
                    // other tables as needed
                )
                    .execute()
            }
        }
    }

    override fun close() {
        dslContext.close()
        dataSource.close()
    }
}
